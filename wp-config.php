<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'islah' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'pa&Ht5I5Aq8M%1oS3]?4pE2{mXu?)7rE,eymN7-Pf}itPNE:?(puqh-~V~#VFD}M' );
define( 'SECURE_AUTH_KEY',  ';OCxUWQUc+ZlZs|~%@Oo#4<5J)Ag`A,:8~qgw,;7x<JU#]yjb`|&o7<g=yfGfjwQ' );
define( 'LOGGED_IN_KEY',    '[R}M)h&4)U(h:HDzK=B*gur=ho=:alv;~;P51haSZl{^Fs@3orgkd/z-u^M:xdV%' );
define( 'NONCE_KEY',        'bo ;wIUWvoxgaIn.&uG8E$90C({mOE`6KaYWn?ZNpC8LyT,hsnk9<xWHjI/6fPuf' );
define( 'AUTH_SALT',        'f`^4%coc}`rqLt@WyIYn*Yo8EOO|U9{30e!ne^7]D8Ffwrohb{P-jQ,/dRK!58`c' );
define( 'SECURE_AUTH_SALT', ';~[Rau^.L*h:;-SY~EYZ)tuZlr/Y0F!JxX;d1= 9ee2UbTtpk*wEKp/_ zm=)9C8' );
define( 'LOGGED_IN_SALT',   'a^8xQ>83&]L}9L()TApmaaKj/bS6:Le)Uq%4hgu_OKqQ?||Z/J:/X/7gEx#%Gf]^' );
define( 'NONCE_SALT',       ' b%r+)hVQ[<IZ0]S8tk>Flh;X.6-b7W$K&!cQ|?np^Q8=q*LFa.nz0*$lf[jR(71' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
