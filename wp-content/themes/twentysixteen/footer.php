<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<?php if ( has_nav_menu( 'primary' ) ) : ?>
				<nav class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Primary Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'menu_class'     => 'primary-menu',
							)
						);
					?>
				</nav><!-- .main-navigation -->
			<?php endif; ?>

			<?php if ( has_nav_menu( 'social' ) ) : ?>
				<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu(
							array(
								'theme_location' => 'social',
								'menu_class'     => 'social-links-menu',
								'depth'          => 1,
								'link_before'    => '<span class="screen-reader-text">',
								'link_after'     => '</span>',
							)
						);
					?>
				</nav><!-- .social-navigation -->
			<?php endif; ?>
		

			<div class="site-info">
				<div class="">
			<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img alt="" src="http://localhost/islah/wp-content/uploads/2020/10/25dc40773470d1fe4f12a0e03a9e023d.jpeg" style="display: block; margin: 0 auto;" originals="128" src-orig="https://2.gravatar.com/avatar/25dc40773470d1fe4f12a0e03a9e023d?s=128&amp;d=https%3A%2F%2F2.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D128&amp;r=G" scale="1" width="128" height="128"></a></p>
			</div>
				<?php
					/**
					 * Fires before the twentysixteen footer text for footer customization.
					 *
					 * @since Twenty Sixteen 1.0
					 */
					do_action( 'twentysixteen_credits' );
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
				if ( function_exists( 'the_privacy_policy_link' ) ) {
					the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
				}
				?>
				<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'twentysixteen' ) ); ?>" class="imprint">
					<?php
					/* translators: %s: WordPress */
					/*printf( __( '', 'twentysixteen' ), 'WordPress' );*/
					?>
				</a>
				<br>
				<p class="">
					© <?php echo date("Y"); ?>  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">Veiled Gems - www.veiledgems.com</a><br>

					All Excerpts, Articles, Links, Pdfs and Mp3 files of this website may be used without restrictions, provided that all material utilized is linked back to the original content at  <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">[Veiled Gems - www.veiledgems.com]</a><br>

					Kindly report any broken links to: <a href="mailto:veiledgems786@gmail.com">veiledgems786@gmail.com</a>
				</p>
				
			</div><!-- .site-info -->
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
